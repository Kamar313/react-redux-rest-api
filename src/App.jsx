import React, { Component } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./component/home";
import AllUser from "./component/allUser";
import SignUpProject from "./component/signUpProject";

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/user" element={<AllUser />} />
          <Route path="/createUser" element={<SignUpProject />} />
        </Routes>
      </BrowserRouter>
    );
  }
}
