import React, { Component } from "react";
import { NavLink } from "react-router-dom";
export default class SignUpProject extends Component {
  render() {
    return (
      <>
        <div className="w-1/1 flex flex-col items-center">
          <div className=" w-1/1 self-start">
            <NavLink to="/">
              <button className=" ml-4 mt-4 bg-slate-400 border-2 border-black h-10 w-32 rounded-lg">
                Go To Home
              </button>
            </NavLink>
          </div>
          <div className=" bg-slate-200 w-96 h-96 mt-4 rounded-lg text-center">
            <input
              type="text"
              placeholder="Enter Your Email"
              className=" h-10 mt-20 w-4/6 pl-4 rounded-lg"
            ></input>
            <input
              type="text"
              placeholder="Enter Your PassWord"
              className=" h-10 mt-14 w-4/6 pl-4 rounded-lg"
            ></input>
            <h3 className=" mt-14 bg-black text-white text-xl w-36 h-8 rounded-lg mx-32">
              Create Account
            </h3>
          </div>
        </div>
      </>
    );
  }
}
