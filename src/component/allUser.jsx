import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { dataFetch, singleFetch, backToHome } from "../store/action/action";

class AllUser extends Component {
  constructor(props) {
    super(props);
  }

  fetchdata = () => {
    this.props.dataFetch();
  };
  singleFetchdata = () => {
    this.props.singleFetch();
  };
  backToHome = () => {
    this.props.backToHome();
  };
  render() {
    return (
      <>
        {console.log(this.props.data.datManupulate)}

        <div>
          <NavLink to="/">
            <button
              onClick={this.backToHome}
              className=" ml-4 mt-4 bg-slate-500 border-2 border-black h-10 w-32 rounded-lg"
            >
              Go To Home
            </button>
          </NavLink>
        </div>
        <div className=" text-center pt-20">
          <button
            onClick={this.fetchdata}
            className=" bg-slate-500 border-2 border-black h-10 w-44 rounded-lg"
          >
            Fetch All User
          </button>
          <button
            onClick={this.singleFetchdata}
            className="bg-slate-500 border-2 ml-4 border-black h-10 w-44 rounded-lg"
          >
            Fetch Random User
          </button>
        </div>
        <div className=" flex flex-wrap justify-center mt-4">
          {this.props.data.datManupulate.loading ? "loading..." : ""}
          {this.props.data.datManupulate.loading == false
            ? this.props.data.datManupulate.data.data.map((ele, i) => {
                return (
                  <div
                    className=" ml-4 bg-slate-100 w-64 mb-4 h-72 text-center rounded-xl"
                    key={i}
                  >
                    <img
                      src={ele.avatar}
                      alt="profile"
                      className="mx-16 mt-2 rounded-full h-32 w-32"
                    />
                    <h2 className=" mt-4 text-2xl font-bold">
                      {ele.first_name}
                    </h2>
                    <h2 className=" mt-4">{ele.email}</h2>
                  </div>
                );
              })
            : ""}
        </div>
      </>
    );
  }
}

function mapStateToProps(state) {
  return { data: state };
}
export default connect(mapStateToProps, { dataFetch, singleFetch, backToHome })(
  AllUser
);
