import React, { Component } from "react";
import { NavLink } from "react-router-dom";
export default class Home extends Component {
  render() {
    return (
      <>
        <div>
          <div className=" text-center py-4 bg-black">
            <h1 className=" text-5xl text-white">Handling API With Redux</h1>
          </div>
          <h3 className=" text-2xl ">Select Option Below</h3>
          <div className=" flex justify-center items-center h-96">
            <NavLink to="/user">
              <button className=" mr-5 h-12 w-28 bg-slate-500 border-2 border-black rounded-lg">
                Get All Users
              </button>
            </NavLink>
            <NavLink to="/createUser">
              <button className=" h-12 w-28 bg-slate-500 border-2 border-black rounded-lg">
                Create
              </button>
            </NavLink>
          </div>
        </div>
      </>
    );
  }
}
