import { combineReducers } from "redux";

const initial = {
  loading: false,
  data: null,
  name: "",
  email: "",
};

const datManupulate = (state = initial, action) => {
  switch (action.type) {
    case "ALL_USER":
      return { ...state, loading: false, data: action.payload };
    case "SINGLE_USER":
      return { ...state, loading: false, data: action.payload };
    case "Back_Button":
      return { ...state, loading: true, data: null };
    case "ADD_USER":
      return { ...state, name: action.payload, email: action.payload };
    default:
      return { ...state, loading: true };
  }
};

export default combineReducers({ datManupulate });
