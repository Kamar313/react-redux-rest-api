export let dataFetch = () => (dispatch) => {
  fetch(`https://reqres.in/api/users?page=2`)
    .then((data) => data.json())
    .then((data) => dispatch({ type: "ALL_USER", payload: data }));
};

export let singleFetch = () => (dispatch) => {
  fetch(`https://reqres.in/api/users/${1}`)
    .then((data) => data.json())
    .then((data) => {
      let data1 = {
        data: [data.data],
      };
      dispatch({ type: "SINGLE_USER", payload: data1 });
    });
};

export let backToHome = () => {
  return { type: "Back_Button" };
};

export let adduser = () => {};
